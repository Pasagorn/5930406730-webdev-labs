var http = require("http");
var request1 = require("request");


request1.get("http://www.kku.ac.th/ikku/api/activities/services/topActivity.php"
    , (error, response, body) => {
        if (error) {
            return console.dir(error);
        }
        var resultObj = JSON.parse(body);

        http.createServer(function (req, res) {
            res.writeHead(200, {
                'Content-Type': 'text/html ;  charset=utf-8'
            });
            res.write('<table>');
            var length = resultObj.activities.length;
            for (var i = 0; i < length; i++) {
                var date = resultObj.activities[i].dateSt;
                var title = resultObj.activities[i].title;
                var url = resultObj.activities[i].url;
                var pnumber = resultObj.activities[i].contact.phone;
                res.write('<tr>');
                res.write('<td>' + [i + 1] + '.' + '</td>');
                res.write('<td>' + date + '</td>');
                res.write('<td>' + '<a href=' + url + '>' + title + '</a>' + '</td>');
                res.write('<td>' + pnumber + '</td>');
                res.write('</tr>');
            } res.write('</table');
            res.end();
        }).listen(8000);
    })



