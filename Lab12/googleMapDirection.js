var request1 = require("request");
var http = require("http");
var i;

request1.get("http://localhost:8000/direction.json", (error, Response, body) => {
    if(error) {
        return console.dir(error);
    }
    resultObj = JSON.parse(body);

    http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    response.write('<h2>Direction from Khon Kaen to Bangkok</h2>')
    response.write('<ol>');
    for(i=0;i<resultObj.routes[0].legs[0].steps.length;i++) {
        response.write('<li>');
        response.write(resultObj.routes[0].legs[0].steps[i].html_instructions + '<br>');
        response.write(resultObj.routes[0].legs[0].steps[i].distance.text);
        response.write('</li>');
    }
}).listen(8081);


console.log('Server running at http://127.0.0.1:8081/');
});